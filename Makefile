PROJECT  = g8_estrutura_salarial
TEX      = pdflatex
BIBTEX   = bibtex
OPTIONS  = -synctex=1 --interaction=nonstopmode -file-line-error
FIGURES := $(shell find figures/* -type f)

develop:
	@latexmk -pvc -silent -pdflatex="$(TEX) $(OPTIONS) %O %S" $(PROJECT).tex

pdf:
	latexmk -pdf -pdflatex="$(TEX) $(OPTIONS) %O %S" $(PROJECT).tex

clean:
	@echo "Cleaning..."
	@curl https://raw.githubusercontent.com/nelsonmestevao/spells/master/art/maid.ascii
	@latexmk -C -silent
	@echo "...✓ done!"
