// Lic Economia
// Eco Trab 
// 2016-2017

clear all
set more off
set rmsg on

cd C:\Users\mangelo\alunos\2017


capture log close
log using analise_g8.txt, text replace

	use C:\data\g1\base\trab_12, clear
	unique ntrab

		merge m:1 EMP_ID ano using C:\data\g1\base\emp_12
			drop _merge
			
			
			tab1 ctrem sitpro
			
			keep if ctrem == 0
			keep if sitpro == 3
			
			
	unique ntrab			
	
	duplicates tag ntrab,gen(dup)
		tab dup
		keep if dup == 0
		drop dup
		
		
		gen mulher = (sexo == 2)
		
		tab1 habil habil1 habil2
		
		
gen educ = 0 if habil == 111
	replace educ = 2 if habil == 112
	replace educ = 4 if habil2 == 21
	replace educ = 6 if habil2 == 22
	replace educ = 9 if habil2 == 23
	replace educ = 12 if habil2 == 31
	replace educ = 13 if habil2 == 40
	replace educ = 14 if habil2 == 50
	replace educ = 15 if habil2 == 60
	replace educ = 16 if habil2 == 70
	replace educ = 19 if habil2 == 80
	drop if educ == .
			
			
		
		gen exper = idade - educ - 6
		
			drop if exper < 0
	
		gen salario = rbase + prest_reg + prest_irreg
			gen salario_h = salario/hnormais
			
			gen lnhw = ln(salario_h)
			
			
		regress lnhw educ c.exper##c.exper mulher
*Classificação STEM
capture drop stem
gen stem=.
replace stem=0 if educ==12  
replace stem=1 if (isced2d==42 | isced2d==44 | isced2d==46 | isced2d==48 | isced2d==52 ) 
*contrariamente a CEPS WP (2015), nao consideramos as categorias e 54 "Manufacturing&Processing" como STEM
replace stem=2 if uni==1 & stem==.  					
label define stemlabels   0 "non-grad. (HS completed)" 1 "STEM grads." 2 "other grads.", replace
label values stem stemlabels

		save tmp, replace
		
			use tmp, clear
			
			keep if habil1 >= 6 & habil1 ~= .
		
		sum idade, detail
		
		regress lnhw b6.habil1 c.exper##c.exper mulher
		
		keep if idade >23 & idade < 30
		sum idade, detail
		
regress lnhw b6.habil1 c.exper##c.exper mulher
	
	tab habil1
	
// Economia

			use tmp, clear
			
			keep if habil1 >= 6 & habil1 ~= .
		
		sum idade, detail
		
		keep if idade >23 & idade < 35
		sum idade, detail
		
keep if habil1 == 6
tab habil, sort
		
		regress lnhw b6.habil1 c.exper##c.exper mulher
		
		regress lnhw b634.habil c.exper##c.exper mulher
	
	tab habil1
	tab caemp
	tab caem2
	tab habil if habil==634 | habil==734 | habil==834
	
	gen lnvendas = ln(vn)
	
	
reg lnhw habil c.exper##c.exper mulher lnvendas

tostring habil,gen(habil_str)
gen area = substr(habil_str,2,.)

keep if habil1 >= 6
tab area


reg lnhw i.habil c.exper##c.exper mulher lnvendas if area == "48"

// solução
capture log close
log using analise_g8.txt, text replace

set rmsg on

timer on 1

use tmp, clear

sum
tab1 mulher habil

keep if idade >23 & idade < 35
keep if habil1>=6
gen lnvendas = ln(vn)
tostring habil,gen(habil_str)
gen area = substr(habil_str,2,.)
levelsof area,local(aa)




foreach asd of local aa {
	di _new(3) "ÁREA:	`asd'" _new(1)
	tab habil if area == "`asd'"
	
	timer on 2
	reg lnhw i.habil c.exper##c.exper mulher lnvendas if area == "`asd'"
	timer off 2
	timer list 2
	timer clear 2
		outreg using tex\resultados_`asd', tex replace

}

timer off 1
timer list 1

log close
