*! version 2.0                   <06May2017>                   Eduardo & Nelson
*! version 1.0                   <26Apr2017>                     Miguel Portela
* ==============================================================================
* Student Name: Eduardo Ribeiro                          Student Number: A76336
* Student Name: Nelson Estevão                           Student Number: A76434
* ------------------------------------------------------------------------------
* Universidade do Minho
* Escola de Economia e Gestão
* Licenciatura em Economia
* ------------------------------------------------------------------------------
* Labour Economics                                        Ano Lectivo 2016/2017
* A Estrutura Salarial
* ==============================================================================

clear all
set more off 	// does not pause when review window is full
set rmsg on   // return how long each command took to execute and what time it completed execution

cd C:\Users\mangelo\alunos\2017

capture log close                       // closes any unclosed logs
log using analise_g8.txt, text replace  // use log file with text format

*===============================================================================
* # 1. PREPARE THE DATA
*===============================================================================

	use C:\data\g1\base\trab_12, clear    // clear memory before opening data

		merge m:1 EMP_ID ano using C:\data\g1\base\emp_12 // merging with emp_12
			drop _merge

			tab1 ctrem sitpro

			keep if ctrem == 0
			keep if sitpro == 3

	unique ntrab
		duplicates tag ntrab,gen(dup)
			tab dup
			keep if dup == 0
			drop dup

		tab1 habil habil1 habil2

	gen educ = 0 if habil == 111
		replace educ = 2 if habil == 112
		replace educ = 4 if habil2 == 21
		replace educ = 6 if habil2 == 22
		replace educ = 9 if habil2 == 23
		replace educ = 12 if habil2 == 31
		replace educ = 13 if habil2 == 40
		replace educ = 14 if habil2 == 50
		replace educ = 15 if habil2 == 60
		replace educ = 16 if habil2 == 70
		replace educ = 19 if habil2 == 80
		drop if educ == .

* Classificação STEM
	capture drop stem
	gen stem=.
		replace stem=0 if educ==12
		replace stem=1 if (isced2d==42 | isced2d==44 | isced2d==46 | isced2d==48 | isced2d==52)
		*contrariamente a CEPS WP (2015), nao consideramos as categorias e 54 "Manufacturing&Processing" como STEM
		replace stem=2 if uni==1 & stem==.

	gen mulher = (sexo == 2)
	gen exper = idade - educ - 6
		drop if exper < 0
	gen salario = rbase + prest_reg + prest_irreg
	gen salario_h = salario/hnormais
	gen lnhw = ln(salario_h)
	gen lnvolume = ln(vn)

	label var educ "Anos de educação"
	label var exper "Experiência em anos"
	label var salario "Salário Mensal"
	label var salario_h "Salário Hora"
	label var lnhw "Logaritmo natural do salário hora"
	label var lnvolume "Logaritmo natural do volume de negócios"

	label define lblmulher   0 "[0] Homem" 1 "[1] Mulher", replace // create a value label
		label values mulher lblmulher // apply the value label
	label define lblstem   0 "[0] non-grad. (HS completed)" 1 "[1] STEM grads." 2 "[2] other grads.", replace // create a value label
		label values stem lblstem // apply the value label

	save tmp, replace

*===============================================================================
* # 2. DESCRIPTIVE STATISTICS
*===============================================================================

	use tmp, clear
		summarize
		describe
		codebook, compact
		tab1 mulher habil ctpro sitpro reg_dur ctrem vndesc1 vndesc2

	keep if idade >23 & idade < 35

		summarize
		describe
		codebook, compact
		tab1 mulher habil ctpro sitpro reg_dur ctrem vndesc1 vndesc2

// Economia

	tab habil1
	tab caemp
	tab caem2
	tab habil if habil==634 | habil==734 | habil==834


*===============================================================================
* # 3. REGRESSIONS
*===============================================================================

	use tmp, clear
		keep if idade >23 & idade <35

	regress lnhw educ c.exper##c.exper mulher lnvolume
	regress lnhw b6.habil1 c.exper##c.exper mulher lnvolume
	regress lnhw b634.habil c.exper##c.exper mulher lnvolume

	keep if habil1>=6 & habil1 ~=.
		tostring habil,gen(habil_str)
		gen area = substr(habil_str,2,.)
		levelsof area,local(aa)

foreach asd of local aa {
		di _new(3) "ÁREA:	`asd'" _new(1)
		tab habil if area == "`asd'"
		reg lnhw i.habil c.exper##c.exper mulher lnvendas if area == "`asd'"
			outreg using tables\resultados_`asd', tex se replace
}

log close 	  // close the log file

exit, clear 	// exit without saving changes

><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><

Notes:
1.
