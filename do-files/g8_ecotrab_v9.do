*! version 9.0                   <15Jun2017>                   Eduardo & Nelson
* ==============================================================================
* Student Name: Eduardo Ribeiro                          Student Number: A76336
* Student Name: Nelson Estevão                           Student Number: A76434
* ------------------------------------------------------------------------------
* Universidade do Minho
* Escola de Economia e Gestão
* Licenciatura em Economia
* ------------------------------------------------------------------------------
* Labour Economics                                          Ano Lectivo 2016/17
* A Estrutura Salarial
* ==============================================================================

clear all
set more off 	// does not pause when review window is full
set rmsg on   // return how long each command took to execute and what time it completed execution

cd C:\Users\mangelo\alunos\2017

global c_dir "C:\Users\mangelo\alunos\2017"          // File path for folders with do-files
global d_dir "C:\data\g1\base"                       // File path for folders with data
global l_dir "C:\Users\mangelo\alunos\2017\logs"     // File path for folders with log-files
global p_dir "C:\Users\mangelo\alunos\2017\plots"    // File path for folders with plots
global t_dir "C:\Users\mangelo\alunos\2017\tables"   // File path for folders with tables

capture log close                       // closes any unclosed logs
log using "$l_dir\analise_g8.txt", text replace  // use log file with text format

*===============================================================================
* # 1. PREPARE THE DATA
*===============================================================================

	use "$d_dir\trab_12", clear    // clear memory before opening data

		merge m:1 EMP_ID ano using "$d_dir\emp_12" // merging with emp_12
			drop _merge

			tab1 ctrem sitpro

			keep if ctrem == 0
			keep if sitpro == 3

	unique ntrab
		duplicates tag ntrab,gen(dup)
			tab dup
			keep if dup == 0
			drop dup

		tab1 habil habil1 habil2

	gen educ = 0 if habil == 111
		replace educ = 2 if habil == 112
		replace educ = 4 if habil2 == 21
		replace educ = 6 if habil2 == 22
		replace educ = 9 if habil2 == 23
		replace educ = 12 if habil2 == 31
		replace educ = 13 if habil2 == 40
		replace educ = 14 if habil2 == 50
		replace educ = 15 if habil2 == 60
		replace educ = 16 if habil2 == 70
		replace educ = 19 if habil2 == 80
		drop if educ == .
/*
* Classificação STEM
	capture drop stem
	gen stem=.
		replace stem=0 if educ==12
		replace stem=1 if (isced2d==42 | isced2d==44 | isced2d==46 | isced2d==48 | isced2d==52)
		*contrariamente a CEPS WP (2015), nao consideramos as categorias e 54 "Manufacturing&Processing" como STEM
		replace stem=2 if uni==1 & stem==.

	label define lblstem   0 "[0] non-grad. (HS completed)" 1 "[1] STEM grads." 2 "[2] other grads.", replace // create a value label
			label values stem lblstem // apply the value label
*/
	gen mulher = (sexo == 2)
	gen exper = idade - educ - 6
		drop if exper < 0
	gen salario = rbase + prest_reg + prest_irreg
	gen salario_h = salario/hnormais
	gen lnhw = ln(salario_h)
	gen lnvolume = ln(vn)

	label var educ "Anos de educação"
	label var exper "Experiência em anos"
	label var salario "Salário Mensal"
	label var salario_h "Salário Hora"
	label var lnhw "Logaritmo natural do salário hora"
	label var lnvolume "Logaritmo natural do volume de negócios"

	label define lblmulher   0 "[0] Homem" 1 "[1] Mulher", replace // create a value label
		label values mulher lblmulher // apply the value label

	save tmp, replace

*===============================================================================
* # 2. DESCRIPTIVE STATISTICS
*===============================================================================

	use tmp, clear
	//drop if mi(salario salario_h educ exper mulher vn lnvn)


	keep if idade >23 & idade < 35
	drop if rbase < 485 | rbase > 20000

	regress lnhw b634.habil mulher lnvolume
		keep if e(sample)
	regress lnhw educ mulher lnvolume
		keep if e(sample)

	summarize
		describe
		codebook, compact
		tab1 mulher habil habil1 habil2 sitpro reg_dur ctrem vndesc1 vndesc2
		tab habil1 mulher

	sum salario, detail
		scalar p95 = r(p99)
	twoway (kdensity salario if mulher == 0 & salario < p95) || (kdensity salario if mulher == 1 & salario < p95, lpattern(dash)), ///
			legend(label(1 "Homem") label(2 "Mulher") region(color(white))) title("Distribuição do salário por sexo") ///
			xtitle("Salário") ytitle("Densidade") scheme(sj) graphregion(color(white)) bgcolor(white)

			graph export salario_genero.png, replace

	sum salario_h, detail
		scalar p95 = r(p99)
	twoway (kdensity salario_h if mulher == 0 & salario_h < p95) || (kdensity salario_h if mulher == 1 & salario_h < p95, lpattern(dash)), ///
			legend(label(1 "Homem") label(2 "Mulher") region(color(white))) title("Distribuição do salário por sexo") ///
			xtitle("Salário/hora") ytitle("Densidade") scheme(sj) graphregion(color(white)) bgcolor(white)

			graph export salario_h_genero.png, replace

			xtile decil_salario = salario_h,nq(10)
			egen median = median(salario_h),by(decil_salario)
			graph box salario_h, over(decil_salario) nooutsides scheme(sj)
			//graph box salario_h, over(decil_salario) nooutsides scheme(sj) || (line median decil)
				graph export box1.png, replace

preserve
drop if decil_salario == 10
			graph box salario_h, over(decil_salario) nooutsides scheme(sj)
				graph export box2.png, replace

restore

// densidade por nível de escolaridade

preserve
keep if habil1 == 3 | habil1 == 6 | habil1 == 7
	sum salario_h, detail
		scalar p95 = r(p99)
	twoway (kdensity salario_h if habil1 == 3 & salario_h < p95) || (kdensity salario_h if habil1 == 6 & salario_h < p95, lpattern(dash)) ///
			|| (kdensity salario_h if habil1 == 7 & salario_h < p95, lpattern("_")), ///
			legend(label(1 "Secundário") label(2 "Licenciatura") label(3 "Mestrado") region(color(white))) title("Distribuição do salário por sexo") ///
			xtitle("Salário/hora") ytitle("Densidade") scheme(sj) graphregion(color(white)) bgcolor(white)

			graph export salario_h_escol.png, replace

restore


// export the descriptive statistics table
		preserve
			//drop if mi(salario salario_h educ exper mulher vn lnvn) // droping observations with missing values
			keep salario salario_h educ exper mulher vn
			order salario salario_h educ exper mulher vn
			outreg2 using "$t_dir\summary_statistics.tex", sum(detail) tex eqkeep(N mean p10 p50 p90 sd min max) replace
		restore

// produce graphs and export


*===============================================================================
* # 3. REGRESSIONS
*===============================================================================

	use tmp, clear
		keep if idade >23 & idade <35

	regress lnhw educ mulher lnvolume
	regress lnhw b3.habil1 mulher lnvolume
	regress lnhw b31.habil2 mulher lnvolume
	regress lnhw b315.habil mulher lnvolume
	regress lnhw b6.habil1 mulher lnvolume
	regress lnhw b60.habil2 mulher lnvolume
	regress lnhw b634.habil mulher lnvolume

preserve
	keep if habil1>=3 & habil1~=. & habil1~=5 & habil1~=4
	keep if habil>=315
		tostring habil,gen(habil_str)
		gen area = substr(habil_str,2,.)

		levelsof area,local(aa)

foreach asd of local aa {
		di _new(3) "ÁREA:	`asd'" _new(1)
		//	tab habil if area == "`asd'"
	reg lnhw i.habil mulher lnvolume if area == "`asd'" // FALTA CORRIGIR AQUI
		gen i = e(sample)
			outreg using "$t_dir\secundario_licenciatura_mestrado_`asd'", tex se replace
		tab habil mulher if area == "`asd'" & i == 1 // FAZER POR BY SEXO ,by(mulher)
		drop i
}
restore

	keep if habil1>=6 & habil1 ~=.
		tostring habil,gen(habil_str)
		gen area = substr(habil_str,2,.)

levelsof area,local(aa)

foreach asd of local aa {
		di _new(3) "ÁREA:	`asd'" _new(1)
		//	tab habil if area == "`asd'"
		reg lnhw i.habil mulher lnvolume if area == "`asd'"
		gen i = e(sample)
			outreg using "$t_dir\licenciatura_mestrado_doutoramento_`asd'", tex se replace
			tab habil mulher if area == "`asd'" & i == 1
			drop i
}

log close 	  // close the log file

/*
exit, clear 	// exit without saving changes

><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><

Notes:
1.

*/
