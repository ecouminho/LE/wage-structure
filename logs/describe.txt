---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
      name:  <unnamed>
       log:  C:\data\g1\base\describe.txt
  log type:  text
 opened on:   4 Apr 2017, 11:23:28

. des

Contains data from C:\data\g1\base\trab_12.dta
  obs:     2,617,333                          
 vars:            39                          24 Apr 2014 18:20
 size:   319,314,626                          
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
              storage   display    value
variable name   type    format     label      variable label
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
ano             int     %8.0g                 Ano de refer�ncia
EMP_ID          long    %12.0g                ID da empresa
ESTAB_ID        long    %12.0g                ID do estabelecimento
nuemp           long    %12.0g                N�mero da empresa (liga��o com a s�rie QP at� 2009)
nuest           long    %12.0g                N�mero do estabelecimento (liga��o com a s�rie QP at� 2009)
ntrab           long    %12.0g                N�mero do Trabalhador
nacio           str2    %2s                   Nacionalidade
sexo            byte    %8.0g      SEXO       Sexo
dt_nasc         long    %tdD_m_Y              Data de nascimento (m�s ano)
idade           byte    %8.0g                 Idade
dt_adm          long    %tdD_m_Y              Data de admiss�o (m�s ano)
antig           byte    %8.0g                 Antiguidade na empresa
dt_prom         long    %tdD_m_Y              Data da �ltima promo��o (m�s ano)
irct            long    %12.0g     IRCT       Instrumento de Regulamenta��o Colectiva do Trabalho (IRCT)
ctpro           long    %12.0g     CTPRO      Categoria profissional
sitpro          byte    %8.0g      SITPRO     Situa��o na profiss�o
tipo_contr      byte    %8.0g      TIPO_CON   Tipo de contrato
tipo_contr1     byte    %8.0g      V18_A      Tipo de contrato (n�vel 1)
reg_dur         byte    %8.0g      REG_DUR    Regime de dura��o do trabalho
habil1          byte    %8.0g      HABIL1     Habilita��es liter�rias (1 d�gito)
habil2          byte    %8.0g      HABIL2     Habilita��es liter�rias (2 d�gitos)
habil           int     %8.0g      HABIL      Habilita��es liter�rias
nqual1          byte    %8.0g      NQUAL1     N�vel de qualifica��o
prof            str5    %5s                   Classifica��o Portuguesa de Profiss�es (CPP 2010 - 5 d�gitos)
prof_4d         str4    %4s                   Classifica��o Portuguesa de Profiss�es (CPP 2010 - 4 d�gitos)
prof_3d         str3    %3s                   Classifica��o Portuguesa de Profiss�es (CPP 2010 - 3 d�gitos)
prof_2d         str2    %2s                   Classifica��o Portuguesa de Profiss�es (CPP 2010 - 2 d�gitos)
prof_1d         str1    %1s                   Classifica��o Portuguesa de Profiss�es (CPP 2010 - 1 d�gito)
ctrem           byte    %39.0g     CTREM      Controle de remunera��o
rbase           double  %12.0g                Remunera��o base paga (euros)
rganho          double  %12.0g                Remunera��o ganho (euros)
rextra          double  %12.0g                Remunera��o suplementar (euros)
prest_reg       double  %12.0g                Presta��es regulares (euros)
prest_irreg     double  %12.0g                Presta��es irregulares (euros)
esc_rem_base    byte    %8.0g      ESC_REM    Escal�o de remunera��o mensal base
esc_rem_ganho   byte    %8.0g      V36_A      Escal�o de remunera��o mensal ganho
pnt             float   %9.0g                 Per�odo normal de trabalho semanal (PNT) (horas)
hnormais        int     %8.0g                 Horas mensais remuneradas - normais (horas)
hextra          int     %8.0g                 Horas mensais remuneradas - suplementares (horas)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Sorted by: 

. use emp_12

. des

Contains data from emp_12.dta
  obs:       274,388                          
 vars:            35                          
 size:    27,164,412                          
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
              storage   display    value
variable name   type    format     label      variable label
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
ano             int     %8.0g                 Ano de refer�ncia
EMP_ID          long    %12.0g                ID da empresa (ano 2010 e seguintes)
nuemp           long    %12.0g                N�mero da empresa (liga��o com a s�rie QP at� 2009)
ancon           str6    %6s                   Ano/m�s de constitui��o
antiguidade     int     %8.0g                 Antiguidade da empresa (anos)
esc_antig_emp   byte    %8.0g      ESC_ANTI   Escal�o de antiguidade da empresa (anos)
natju           byte    %8.0g      NATJU      Natureza jur�dica
csoc            double  %12.0g                Capital Social (euros)
cspri           double  %12.0g                Capital Social privado nacional (%)
cspub           double  %12.0g                Capital Social p�blico nacional (%)
csest           double  %12.0g                Capital Social estrangeiro (%)
csocesc         byte    %8.0g      CSOCESC    Escal�o do Capital Social
nut1_emp        byte    %8.0g      NUT1_EMP   NUT I da empresa em 31 de outubro
nut2_emp        byte    %8.0g      NUT2_EMP   NUT II da empresa em 31 de outubro
nut3_emp        str3    %3s                   NUT III da empresa em 31 de outubro
dtemp           byte    %8.0g      DTEMP      Distrito da empresa em 31 de outubro
ldemp           int     %8.0g      LDEMP      Concelho da empresa em 31 de outubro
fregemp         long    %12.0g     FREGEMP    Freguesia da empresa em 31 de outubro
caem1l          str1    %1s                   Actividade Econ�mica da empresa (CAE_Rev.3 - 1 letra)
caem2           byte    %8.0g      CAEM2      Actividade Econ�mica da empresa (CAE_Rev.3 - 2 d�gitos)
caem3           int     %8.0g      CAEM3      Actividade Econ�mica da empresa (CAE_Rev.3 - 3 d�gitos)
caem4           int     %8.0g      CAEM4      Actividade Econ�mica da empresa (CAE_Rev.3 - 4 d�gitos)
caemp           long    %12.0g     CAEMP      Actividade Econ�mica da empresa (CAE_Rev.3 - 5 d�gitos)
vn              double  %12.0g                Volume de Neg�cios (euros)
vn_ano          int     %8.0g                 Ano de refer�ncia do Volume de Neg�cios
vndesc1         byte    %8.0g      VNDESC1    Escal�o de Volume de Neg�cios
vndesc2         byte    %8.0g      VNDESC2    Escal�o de Volume de Neg�cios
nest            int     %8.0g                 N�mero de estabelecimentos
pemp            int     %8.0g                 N�mero de pessoas ao servi�o da empresa (31 de outubro)
escdim1         byte    %8.0g      ESCDIM1    Escal�o de dimens�o da empresa (31 de outubro)
escdim2         byte    %8.0g      ESCDIM2    Escal�o de dimens�o da empresa (31 de outubro)
pempl           int     %8.0g                 N�mero de pessoas ao servi�o da empresa (outubro)
escdim_linhas1  byte    %8.0g      ESCDIM_L   Escal�o de dimens�o da empresa (outubro)
escdim_linhas2  byte    %8.0g      V34_A      Escal�o de dimens�o da empresa (outubro)
tcoemp          int     %8.0g                 N�mero de trabalhdores por conta de outrem (TCO) (outubro)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Sorted by: 

.   log close
      name:  <unnamed>
       log:  C:\data\g1\base\describe.txt
  log type:  text
 closed on:   4 Apr 2017, 11:23:45
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
